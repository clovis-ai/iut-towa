/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

/**
 * Une classe pour déterminer la portée d'une zone.
 * <p>Cette classe est utilisée pour optimiser la recherche des cases : on
 * n'effectue la recherche qu'une fois même si elle est nécessaire pour plusieurs
 * actions.
 * @author Ivan Canet
 */
public class Zone {
    
    private final Case[][] plateau;
    private final int ligne, colonne;
    private boolean contour;
    private Case[] portee = null;
    
    /**
     * Initialise cet objet. Le constructeur ne fait aucun calcul ou traitement.
     * @param contexte données sur l'analyse en cours
     */
    public Zone(Contexte contexte){
        this.plateau = contexte.plateau();
        this.ligne = contexte.ligne();
        this.colonne = contexte.colonne();
        this.contour = true;
    }
    
    /**
     * Génère la portée.
     * @see #genererPorteeContour() S'il s'agit d'un contour
     * @see #genererPorteeTotale() S'il s'agit d'une activation ou d'une fusion
     */
    private void genererPortee(){
        if(contour)
            genererPorteeContour();
        else
            genererPorteeTotale();
    }
    
    /**
     * Génère la portée en sélectionnant les cases adjacentes.
     */
    public void genererPorteeContour(){
        System.out.println("Contour de " + ligne + " " + colonne + " :");
        if(portee != null)
            throw new IllegalStateException("La portée de cet objet ne peut pas être calculée une deuxième fois.");
        portee = new Case[8];
        int i = 0;
        for(int x = -1; x <= 1; x++)
            for(int y = -1; y <= 1; y++)
                if(x != 0 || y != 0){
                    portee[i] = trouverTour(plateau, ligne+x, colonne+y, x, y, 1);
                    i++;
                }
    }
    
    /**
     * Si la zone est déjà générée comme contour, permet de l'étendre sur les
     * points cardinaux.
     */
    public void etendrePortee(){
        System.out.println("Extension du contour de " + ligne + " " + colonne + " :");
        contour = false;
        int i = 0;
        for(int x = -1; x <= 1; x++)
            for(int y = -1; y <= 1; y++)
                if(x != 0 || y != 0){
                    if((x == 0 || y == 0) && portee[i] == null)    // pas une diagonale, pas remplie
                        portee[i] = trouverTour(plateau, ligne+2*x, colonne+2*y, x, y, plateau.length);
                    else    // La case a déjà été calculée, ou c'est une diagonale (pas de calcul supplémentaire nécessaire)
                        System.out.println("\tDéjà calculée.");
                    i++;
                }
    }
    
    /**
     * Génère la portée en sélectionnant les diagonales adjacentes et les cases
     * les plus proches pour chaque points cardinaux.
     */
    private void genererPorteeTotale(){
        System.out.println("Portée de " + ligne + " " + colonne + " :");
        if(portee != null)
            throw new IllegalStateException("La portée de cet objet ne peut pas être calculée une deuxième fois.");
        portee = new Case[8];
        int i = 0;
        for(int x = -1; x <= 1; x++)
            for(int y = -1; y <= 1; y++)
                if(x != 0 || y != 0){
                    if(x == 0 || y == 0)    // pas une diagonale
                        portee[i] = trouverTour(plateau, ligne+x, colonne+y, x, y, plateau.length);
                    else                    // diagonale
                        portee[i] = trouverTour(plateau, ligne+x, colonne+y, x, y, 1);
                    i++;
                }
    }
    
    /**
     * Trouve la tour la plus proche dans une direction donnée.
     * @param plateau plateau de jeu.
     * @param ligne ligne du point de départ de la recherche
     * @param colonne colonne du point de départ de la recherche
     * @param dirX direction verticale de la recherche [-1,1]
     * @param dirY direction horizontale de la recherche [-1,1]
     * @param maxDist distance maximale à parcourir
     * @return La tour la plus proche.
     */
    public static final Case trouverTour(Case[][] plateau, int ligne, int colonne, int dirX, int dirY, int maxDist){
        System.out.print(Utils.directionVersUtf(dirX, dirY));
        for(int i = 0; i < maxDist; i++){
            
            if(ligne < 0 || ligne >= plateau.length || colonne < 0 || colonne >= plateau.length){
                break;
            }
            
            Case c = plateau[ligne][colonne];
            if(c.tourPresente){
                System.out.println("\tTrouvé : " + (c.estNoire ? "N" : "B") + c.hauteur + c.altitude);
                return c;
            }
            ligne += dirX;
            colonne += dirY;
        }
        System.out.println("\tHors zone" + (maxDist >= plateau.length ? "" : " (maxDist=" + maxDist + ")"));
        return null;
    }
    
    /**
     * Trouve la tour la plus proche dans une direction donnée, à partir de la position
     * du test actuel.
     * @param ctx données sur le test en cours
     * @param dirX direction verticale de la recherche [-1,1]
     * @param dirY direction horizontale de la recherche [-1,1]
     * @param maxDist distance maximale à parcourir
     * @return La tour la plus proche.
     */
    public static final Case trouverTour(Contexte ctx, int dirX, int dirY, int maxDist){
       return trouverTour(ctx.plateau(), ctx.ligne(), ctx.colonne(), dirX, dirY, maxDist); 
    }
    
    /**
     * Permet d'obtenir les cases impactées par une action.
     * <p>Selon les paramètres donnés au constructeur, cette méthode renvoie les
     * cases adjacentes si présentes OU les cases les plus proches dans chaque
     * direction.
     * @return Un tableau des cases impactées.
     * @see #Zone(towa.Case[][], int, int, boolean) Voir le constructeur
     */
    public Case[] portee(){
        if(portee == null)
            genererPortee();
        return portee;
    }
    
    /**
     * Permet d'obtenir les cases impactées par une action.
     * <p>Permet 
     * @param contour
     * @return Un tableau des cases impactées.
     */
    public Case[] portee(boolean contour){
        if(portee == null){                     //  Cette zone n'est pas générée
            this.contour = contour;             //      On affecte le contour voulu
            genererPortee();                    //      On génère le contour correspondant
        }else if(this.contour && !contour)      //  Cette zone est générée en CONTOUR, mais on souhaite TOTAL
            etendrePortee();                    //      Extension de la portée
        else if(!this.contour && contour)       //  Cette zone est générée en TOTAL, mais on souhaite CONTOUR
            throw new IllegalStateException("Une zone ne peut pas repartir en arrière.");
        else
            System.out.println("Aucun nouveau calcul nécessaire.");
        return portee;
    }
    
    /**
     * Permet de savoir si cette zone ne contient que le contour, ou les cardinalités aussi.
     * @return Cette zone est-elle en mode contour ?
     */
    public boolean estContour(){
        return contour;
    }
    
}
