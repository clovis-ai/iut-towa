package towa;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static towa.Utils.DIRECTIONS;
import static towa.Utils.copie;

/**
 * Joueur implémentant les actions possibles à partir d'un plateau, pour un
 * niveau donné.
 */
public class JoueurTowa implements IJoueurTowa {

    /**
     * Taille d'un côté du plateau (carré).
     */
    final static int TAILLE = 16;

    /**
     * Cette méthode renvoie, pour un plateau donné et un joueur donné, toutes
     * les actions possibles pour ce joueur.
     *
     * @param plateau le plateau considéré
     * @param joueurNoir vrai si le joueur noir joue, faux si c'est le blanc
     * @param niveau le niveau de la partie à jouer
     * @return l'ensemble des actions possibles
     */
    @Override
    public String[] actionsPossibles(Case[][] plateau, boolean joueurNoir, int niveau) {
        // afficher l'heure de lancement
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
        System.out.println("actionsPossibles : lancement le " + format.format(new Date()));
        
        Contexte contexte = new Contexte(plateau, joueurNoir);
        
        //  ANALYSER LES ACTIONS POSSIBLES                                      ANALYSER LES ACTIONS POSSIBLES
        for (int lig = 0; lig < TAILLE; lig++) {
            for (int col = 0; col < TAILLE; col++) {
                contexte.deplacement(lig, col);
                
                // On génère le contour pour la pose
                Zone portee = new Zone(contexte);
                
                // Génération des actions de type position 
                actionsPosition(contexte, portee);
            }
        }
        contexte.deplacement(-1, -1); // On invalide les coordonnées pour être sûr de ne pas les lire par accident
        chatons(contexte);
        
        String[] actions = contexte.actions();
        System.out.println("actionsPossibles : fin, " + actions.length + " actions trouvées avec " + contexte.noirs() + " et " + contexte.blancs());
        return Utils.nettoyerTableau(actions);
    }
    
    /**
     * Teste et génère toutes les actions possibles de type Position :
     * pose, activation, fusion et magie.
     * @param contexte données sur les tests
     * @param portee zone impactée
     */
    void actionsPosition(Contexte contexte, Zone portee){
        pose(contexte, portee);
        activation(contexte, portee);
        fusion(contexte, portee);
        magie(contexte);
    }
    
    /**
     * Gère la pose simple et double des pions.
     * @param ctx données sur les tests
     * @param contour zone impactée
     */
    void pose(Contexte ctx, Zone contour){
        System.out.print("POSE DE " + ctx.ligne() + " " + ctx.colonne());
        final int n = genererPose(ctx, contour);
        if(n != 0){
            System.out.print("Pose de " + n + " jetons ; ");
            Case[][] plateau = copie(ctx.plateau());
            plateau[ctx.ligne()][ctx.colonne()] = new Case(true, ctx.estNoir(), ctx.maitresse().hauteur + n, 0, 0);
            int[] pions = new int[]{ctx.noirs()  + (ctx.estNoir() ? n : 0),
                                    ctx.blancs() + (ctx.estNoir() ? 0 : n)};
            pions = signaux(ctx, pions, plateau);
            ctx.nouvelleAction("P",
                    pions[0],
                    pions[1]);
        }
    }
    
    /**
     * Gère l'activation.
     * @param ctx données sur les tests
     * @param portee zone impactée
     */
    void activation(Contexte ctx, Zone portee){
        if(activationPossible(ctx)){
            System.out.println("ACTIVATION DE " + ctx.ligne() + " " + ctx.colonne());
            int[] pions = genererActivation(ctx, portee);
            ctx.nouvelleAction("A", pions[0], pions[1]);
        }
    }
    
    /**
     * Gère la fusion.
     * @param ctx données sur les tests
     * @param portee zone impactée
     */
    void fusion(Contexte ctx, Zone portee){
        if(fusionPossible(ctx)){
            System.out.println("FUSION DE " + ctx.ligne() + " " + ctx.colonne());
            int[] pions = genererFusion(ctx, portee);
            ctx.nouvelleAction("F", pions[0], pions[1]);
        }
    }
    
    /**
     * Gère la magie.
     * @param ctx données sur les tests
     */
    void magie(Contexte ctx){
        if(magiePossible(ctx))
            ctx.nouvelleAction("M", ctx.noirs(), ctx.blancs());
    }
    
    /**
     * Gère les chatons
     * @param ctx données sur l'état de la partie
     */
    void chatons(Contexte ctx){
        for(char dir : new char[]{'N', 'S', 'O', 'E'}){
            System.out.print("Chatons : ");
            int[] resultats = genererChatons(ctx, dir);
            ctx.nouvelleAction("C", dir, resultats[0], resultats[1]);
        }
    }
    
    /**
     * Génère les signaux.
     * @param ctx données sur l'état de la partie
     * @param pions nombre de pions après la pose
     * @return Nombre de pions en prenant en compte les pions détruits par des signaux.
     */
    int[] signaux(Contexte ctx, int[] pions, Case[][] plateau){
        System.out.println("Calcul des signaux ...");
        List<Case> trouvees = new ArrayList<>();
        for(int[] dir : DIRECTIONS){
            Case cardinale = Zone.trouverTour(plateau, ctx.ligne()+dir[0], ctx.colonne()+dir[1], dir[0], dir[1], TAILLE);
            if(cardinale != null && cardinale.tourPresente && cardinale.estNoire == ctx.estNoir()){
                System.out.println("Un signal est créé ...");
                int[][] dirInverse = new int[2][2];
                dirInverse[0] = new int[]{ dir[1],  dir[0]};
                dirInverse[1] = new int[]{-dir[1], -dir[0]};
                int ligne = ctx.ligne() + dir[0];
                int colonne = ctx.colonne() + dir[1];
                while(!ctx.get(ligne, colonne).tourPresente){
                    Case[] tours = new Case[2];
                    for(int i = 0; i < dirInverse.length; i++){
                        int[] dirActu = dirInverse[i];
                        tours[i] = Zone.trouverTour(plateau, ligne+dirActu[0], colonne+dirActu[1], dirActu[0], dirActu[1], TAILLE);
                    }
                    if(tours[0] != null && tours[1] != null){
                        if(tours[0].estNoire == tours[1].estNoire){
                            System.out.println("Trouvé deux cases !");
                            for(Case tour : tours)
                                if(!trouvees.contains(tour))
                                    trouvees.add(tour);
                            if(!trouvees.contains(cardinale))
                                trouvees.add(cardinale);
                            if(!trouvees.contains(plateau[ctx.ligne()][ctx.colonne()]))
                                trouvees.add(plateau[ctx.ligne()][ctx.colonne()]);
                        }
                    }
                    ligne += dir[0];
                    colonne += dir[1];
                }
            }
        }
        System.out.print("Cases à supprimer : ");
        for(Case c : trouvees){
            System.out.print((c.estNoire ? "N" : "B") + c.hauteur + "" + c.altitude + " ");
            if(c.estNoire)
                pions[0] -= c.hauteur;
            else
                pions[1] -= c.hauteur;
        }
        return pions;
    }

    /**
     * Combien de pions peut-on poser sur une case ?
     * @param ctx données sur les tests en cours
     * @param contour zone impactée en cas de pose double
     * @return 0 si on ne peut pas jouer, 1 si on peut poser un pion,
     *         2 si la pose double est autorisée.
     */
    int genererPose(Contexte ctx, Zone contour) {
        Case car = ctx.maitresse();
        
        if(car.nature == 0){
            if(car.tourPresente){
                if(car.estNoire == ctx.estNoir()){
                    if(car.hauteur + car.altitude < 4){
                        System.out.println(" Tour alliée, taille autorisée");
                        return 1;
                    }else{
                        System.out.println(" Taille maximale atteinte");
                        return 0;
                    }
                }else{
                    System.out.println(" Tour ennemie");
                    return 0;
                }
            }else{
                if(car.altitude >= 4){
                    System.out.println(" Altitude trop élevée, impossible de jouer dans cette case");
                    return 0;
                }
                System.out.println(" ");
                for(Case c : contour.portee(true))
                    if(c != null){
                        System.out.print(Utils.caseToString(c));
                        if(c.estNoire != ctx.estNoir()){
                            System.out.print("\t✔ Trouvé !");
                            if(car.altitude <= 2){
                                System.out.println(" Pose double.");
                                if(!ctx.estCouvrant() && ctx.estCouvrant(ctx.ligne(), ctx.colonne(), ctx.estNoir())){
                                    System.out.println(" Cette pose rend couvrant !");
                                    return 4 - (car.altitude + car.hauteur);
                                }else
                                    return 2;
                            }else{
                                System.out.println(" Pose interdite.");
                                return 0;
                            }
                        }else{
                            System.out.println("\t= Même couleur");
                        }
                    }
                System.out.print("Pas de pion trouvé,");
                if(!ctx.estCouvrant() && ctx.estCouvrant(ctx.ligne(), ctx.colonne(), ctx.estNoir())){
                    System.out.println(" pose couvrante !");
                    return 4 - (car.altitude + car.hauteur);
                }else{
                    System.out.println(" pose simple.");
                    return 1;
                }
            }
        }else{
            System.out.println(" Case constituée d'eau.");
            return 0;
        }
    }
    
    /**
     * La magie est-elle autorisée dans cette case ?
     * @param ctx données sur les tests
     * @return <code>true</code> si la magie est autorisée.
     */
    boolean magiePossible(Contexte ctx){
        Case source = ctx.maitresse();
        Case but = ctx.get(TAILLE - ctx.ligne() - 1, TAILLE - ctx.colonne() - 1);
        if(source.tourPresente && source.estNoire == ctx.estNoir()){
            System.out.print("MAGIE EN " + ctx.ligne() + " " + ctx.colonne() + " :");
            if(but.nature == 0 && !but.tourPresente){
                if(but.altitude + source.hauteur <= 4){
                    System.out.println(" Possible");
                    return true;
                }else
                    System.out.println(" Impossible : le résultat serait trop haut");
            }else
                System.out.println(" Impossible : la destination n'est pas vide ou est remplie d'eau");
        }
        return false;
    }
    
    /**
     * Peut-on activer la tour présente ?
     * @param ctx données sur les tests
     * @return <code>true</code> si on peut activer cette case.
     */
    boolean activationPossible(Contexte ctx){
        Case car = ctx.maitresse();
        return car.tourPresente
            && car.estNoire == ctx.estNoir();
    }
    
    /**
     * Compte le nombre de jetons qui resteront après l'activation de la tour.
     * @param ctx données sur les tests
     * @param zone zone impactée
     * @return Un tableau contenant :<br>
     *         [0] - Le nombre de pions noirs après l'activation
     *         [1] - Le nombre de pions blancs après l'activation
     */
    int[] genererActivation(Contexte ctx, Zone zone){
        // Compter le nombre de cases modifiées
        Case maitresse = ctx.maitresse();
        int[] pions = ctx.pions();
        
        Case[] portee = zone.portee(false);
        System.out.print("Prises en compte :");
        for(Case c : portee)
            if(c != null)
                if(c.hauteur + c.altitude < maitresse.hauteur + maitresse.altitude && c.estNoire != ctx.estNoir()){
                    System.out.print(" " + Utils.caseToString(c));
                    if(c.estNoire)
                        pions[0] -= c.hauteur;
                    else
                        pions[1] -= c.hauteur;
                }else{
                    System.out.print(" !" + Utils.caseToString(c));
                }
        System.out.println();
        return pions;
    }
    
    /**
     * Peut-on déclencher une fusion ?
     * @param ctx données sur les tests
     * @return <code>true</code> si on peut déclencher une fusion sur cette case.
     */
    boolean fusionPossible(Contexte ctx){
        Case c = ctx.maitresse();
        return c.tourPresente && c.estNoire == ctx.estNoir();
    }
    
    /**
     * Compter le nombre de pions restant après la fusion.
     * @param ctx données sur les tests
     * @param portee zone impactée
     * @return Un tableau contenant :<br>
     *         [0] - Le nombre de pions noirs après l'activation
     *         [1] - Le nombre de pions blancs après l'activation
     * @see #trouverPortee(towa.Case[][], int, int) Comment trouver les cases concernées ?
     */
    int[] genererFusion(Contexte ctx, Zone portee){
        int modifs = 0;
        // Recherche des cases
        System.out.print("Concernées :");
        for(Case c : portee.portee(false))
            if(c != null && c.estNoire == ctx.estNoir()){
                System.out.print(" " + (c.estNoire ? "N" : "B") + c.hauteur);
                modifs += c.hauteur;
            }
        System.out.println("\nJetons détruits : " + modifs);
        Case maitresse = ctx.maitresse();
        
        // Récupérage du bon nombre de pions
        int pionsFinal;
        if(ctx.estNoir()){
            pionsFinal = ctx.noirs();
        }else{
            pionsFinal = ctx.blancs();
        }
        pionsFinal -= modifs;
        
        // Validation de la fusion
        int niveauMaitresse = maitresse.hauteur + maitresse.altitude;
        System.out.println("• " + Utils.caseToString(maitresse));
        if(niveauMaitresse + modifs < 4)
            pionsFinal += modifs;
        else
            pionsFinal += 4 - niveauMaitresse;
        
        System.out.println("Final : " + pionsFinal);
        return ctx.estNoir() ? new int[]{pionsFinal, ctx.blancs()}
                             : new int[]{ctx.noirs(), pionsFinal};
    }
    
    /**
     * Calcule le nombre de cases détruites par des chatons.
     * @param ctx données sur les tests
     * @param dir direction dans laquelle les chatons se déplacent (N|S|O|E)
     * @return Un tableau contenant :<br>
     *         [0] - Le nombre de pions noirs après l'activation
     *         [1] - Le nombre de pions blancs après l'activation
     */
    int[] genererChatons(Contexte ctx, final char dir){
        int lig, col;
        switch(dir){
            case 'N': lig = 1; col = 0; break;
            case 'S': lig = -1; col = 0; break;
            case 'O': lig = 0; col = 1; break;
            case 'E': lig = 0; col = -1; break;
            default: throw new IllegalArgumentException("Caractère inconnu : " + dir);
        }
        System.out.println(Utils.directionVersUtf(lig, col));
        int x, y;
        int nbNoir = ctx.noirs();
        int nbBlanc = ctx.blancs();
        if(dir == 'N' || dir == 'S')
            for(int i = 0; i < TAILLE; i++){
                x = (dir == 'N' ? 0 : TAILLE-1);
                y = i;
                System.out.print(x + " " + y + "\t");
                Case c = Zone.trouverTour(ctx.plateau(), x, y, lig, col, TAILLE);
                if(c != null){
                    if(c.estNoire)
                        nbNoir -= c.hauteur;
                    else
                        nbBlanc -= c.hauteur;
                }
            }
        else
            for(int i = 0; i < TAILLE; i++){
                x = i;
                y = (dir == 'O' ? 0 : TAILLE-1);
                System.out.print(x + " " + y + "\t");
                Case c = Zone.trouverTour(ctx.plateau(), x, y, lig, col, TAILLE);
                if(c != null){
                    if(c.estNoire)
                        nbNoir -= c.hauteur;
                    else
                        nbBlanc -= c.hauteur;
                }
            }
        return new int[]{nbNoir, nbBlanc};
    }
    
    /**
     * Une fonction principale juste là pour que vous puissiez tester votre
     * actionsPossibles() sur le plateau de votre choix. À modifier librement.
     *
     * @param args arguments de la ligne de commande, inutiles ici
     */
    public static void main(String[] args) {
        JoueurTowa joueur = new JoueurTowa();
        // un plateau sur lequel on veut tester actionsPossibles()
        String textePlateau = "  A   B   C   D   E   F   G   H   I   J   K   L   M   N   O   P \n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "a|   |  1|  2|E 2|   |E 1|N22|N31|  2|  4|   |   |  4|E 1|   |B11|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "b|  1|  1|E  |E 4|E 1|   |   |N12|N22|  3|E 4|E  |  4|E  |N21|  1|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "c|  4|N11|N22|  2|  1|B13|E 1|  4|  3|E 2|   |E 2|   |  2|   |E 4|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "d|E 1|E 4|  1|B1 |  2|E 1|E 4|N13|N13|  4|E 2|B11|  2|   |  1|  2|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "e|E  |E 3|  1|E  |  2|E 3|E 4|N13|   |   |  3|  1|E 3|E  |E 4|  2|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "f|  3|E 2|B1 |  1|E 2|  2|N13|  3|   |E 2|   |E  |   |   |  3|E 1|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "g|   |  3|  4|E 4|E 4|  2|   |E  |   |E 3|E  |  1|E  |  3|  3|  1|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "h|  4|E  |E 4|   |  3|  1|  3|   |  3|  4|E 4|E  |  2|E  |E 4|B13|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "i|  1|E 4|  4|E 1|E 1|  1|  3|  1|   |E 1|E 1|E 3|  4|E 4|  4|E  |\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "j|E  |  2|E 1|E  |E  |E 1|   |   |  1|  3|   |E 1|B12|  2|E 4|   |\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "k|E  |N1 |N11|  2|  4|B1 |E 1|E 4|E 3|  1|   |E  |  4|  1|  2|   |\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "l|  2|E 1|B21|E 4|E 2|   |E  |E 4|  1|E 4|E 3|  2|B1 |E 4|  2|E 3|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "m|E  |  1|   |  3|   |  1|  3|  1|B13|  1|  4|   |  3|E 4|  3|   |\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "n|  3|  2|N12|  2|E 2|N13|E 3|E 3|E 2|   |  3|E 4|  4|  3|E 3|E  |\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "o|   |E 2|   |E 2|E 4|  4|E 1|E 2|   |  1|E 1|  1|  2|E 2|E  |   |\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "p|E  |  2|E 4|E  |  1|  3|  3|  4|  3|  1|E 1|  1|E 2|E 1|E 2|  1|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+";
        Case[][] plateau = Utils.plateauDepuisTexte(textePlateau);
        // on choisit la couleur du joueur
        boolean noir = false;
        // on choisit le niveau
        int niveau = 1;
        // on lance actionsPossibles
        String[] actionsPossiblesDepuisPlateau
                = joueur.actionsPossibles(plateau, noir, niveau);

        // on affiche toutes les actions possibles calculées :
        Utils.afficherActionsPossibles(actionsPossiblesDepuisPlateau);

        // on peut aussi tester si une action est dans les actions possibles :
        String action = "PaA,1,0";
        if (Utils.actionsPossiblesContient(actionsPossiblesDepuisPlateau, action)) {
            System.out.println(action + " est calculé comme possible");
        } else {
            System.out.println(action + " est calculé comme impossible");
        }
    }
}
