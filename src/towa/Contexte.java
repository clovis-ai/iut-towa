/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import java.util.ArrayList;
import java.util.List;
import static towa.Utils.numVersCarColonne;
import static towa.Utils.numVersCarLigne;

/**
 * Regroupe toutes les informations sur le test en cours et sur le plateau en une
 * seule location. On retrouve par exemple le nombre de pions de chaque couleur,
 * les coordonnées du test et un pointeur vers le plateau.
 * @author Ivan Canet
 */
public class Contexte {
    
    private final Case[][] plateau;
    private int x=-1, y=-1;
    private final int[] pions;
    private final boolean joueurNoir;
    private final List<String> actions;
    
    private boolean couvrant;
    
    /** Nombre d'actions possibles au maximum. */
    private static final int POSSIBILITES = 16 * 16 * 4 + 4;
    
    /**
     * Crée un objet de type Contexte, contenant des informations sur les tests
     * en cours.
     * <p>Ce constructeur s'occupe aussi de compter le nombre de pions noirs et
     * blancs sur la plateau.
     * <p>Attention : ce constructeur n'initialise pas les coordonnées, pensez à
     * le faire avec {@link #deplacement(int, int) la méthode deplacement(ligne, colonne)}.
     * @param plateau plateau de jeu
     * @param joueurNoir le joueur est-il noir ?
     */
    public Contexte(Case[][] plateau, boolean joueurNoir){
        this.plateau = plateau;
        this.joueurNoir = joueurNoir;
        pions = new int[2];
        compterPions();
        actions = new ArrayList<>(POSSIBILITES);
        couvrant = estCouvrant(plateau);
        System.out.println();
    }
    
    /**
     * Compte le nombre de pions blancs et de pions noirs.
     */
    private void compterPions(){
        System.out.print("Comptage des pions ...");
        
        for(int i = 0; i < pions.length; i++)
            pions[i] = 0;
        
        for(Case[] l : plateau)
            for(Case c : l)
                if(c.tourPresente){
                    if(c.estNoire)
                        pions[0] += c.hauteur;
                    else
                        pions[1] += c.hauteur;
                }
        System.out.println(" " + pions[0] + " noirs, " + pions[1] + " blancs.");
    }
    
    /**
     * L'utilisateur a-t-il le droit d'accéder aux coordonnées ?
     * @throws NullPointerException Si les coordonnées n'ont pas été initialisées avec {@link #deplacement(int, int) }.
     */
    private void autorisationCoordonnees(){
        // Inutile de dire à l'utilisateur que ça correspond à x ou y négatif.
        if(x <= -1 || y <= -1)
            throw new NullPointerException("Les coordonnées n'ont pas été initialisées.");
    }
    
    /**
     * Quelles sont les coordonnées du test en cours ?
     * @return Les coordonnées du test en cours : <br>
     *         [0] Numéro de la ligne <br>
     *         [1] Numéro de la colonne
     * @see #deplacement(int, int) Changer de test
     */
    public int[] coordonnees(){
        autorisationCoordonnees();
        return new int[]{x, y};
    }
    
    /**
     * Déplace le contexte vers le test suivant.
     * @param ligne numéro de ligne du nouveau test
     * @param colonne numéro de colonne du nouveau test
     * @return Renvoie cet objet pour permettre d'enchaîner les méthodes.
     */
    public Contexte deplacement(int ligne, int colonne){
        x = ligne;
        y = colonne;
        return this;
    }
    
    /**
     * Où est situé le test en cours ?
     * @return numéro de la ligne du test en cours
     * @see #deplacement(int, int) Changer de test
     */
    public int ligne(){
        autorisationCoordonnees();
        return x;
    }
    
    /**
     * Où est situé le test en cours ?
     * @return numéro de la ligne du test en cours
     * @see #deplacement(int, int) Changer de test
     */
    public int colonne(){
        autorisationCoordonnees();
        return y;
    }
    
    /**
     * Combien y a-t-il de pions sur le plateau ?
     * @return Nombre de pions sur le plateau :<br>
     *         [0] Nombre de pions noirs<br>
     *         [1] Nombre de pions blancs
     */
    public int[] pions(){
        return new int[]{pions[0], pions[1]};   // Copie pour éviter les effets de bord
    }
    
    /**
     * Combien y a-t-il de pions sur le plateau ?
     * @return Nombre de pions noirs.
     */
    public int noirs(){
        return pions[0];
    }
    
    /**
     * Combien y a-t-il de pions sur le plateau ?
     * @return Nombre de pions blancs.
     */
    public int blancs(){
        return pions[1];
    }
    
    /**
     * De quel couleur est le joueur ?
     * @return <code>true</code> s'il est noir.
     */
    public boolean estNoir(){
        return joueurNoir;
    }
    
    /**
     * Récupère le plateau de jeu.
     * @return Le plateau de jeu.
     */
    public Case[][] plateau(){
        return plateau;
    }
    
    /**
     * La case principale du test en cours.
     * <p>Cette méthode est un raccourci pour :
     * <pre>
     * Contexte.plateau()[Contexte.ligne()][Contexte.colonne()];
     * </pre>
     * @return La case maitresse.
     */
    public Case maitresse(){
        autorisationCoordonnees();
        return plateau[x][y];
    }
    
    /**
     * Récupère une case quelconque du plateau.
     * <p>Cette méthode est un raccourci pour :
     * <pre>
     * Contexte.plateau()[ligne][colonne];
     * </pre>
     * @param ligne ligne de la case qui nous intéresse
     * @param colonne colonne de la case qui nous intéresse
     * @return La case demandée dans le plateau.
     */
    public Case get(int ligne, int colonne){
        return plateau[ligne][colonne];
    }
    
    /**
     * Enregistre une action comme possible.
     * @param action une action sous forme de String
     */
    public void nouvelleAction(String action){
        System.out.println("PROPOSITION : "+action);
        actions.add(action);
    }
    
    /**
     * Enregistre une action de type position.
     * @param nom nom de l'action
     * @param x numéro de ligne
     * @param y numéro de colonne
     * @param nbNoir nombre de pions noir après l'action
     * @param nbBlanc nombre de pions blancs après l'action
     */
    public void nouvelleAction(String nom, int x, int y, int nbNoir, int nbBlanc){
        nouvelleAction(nom + numVersCarLigne(x) + numVersCarColonne(y) + "," + nbNoir + "," + nbBlanc);
    }
    
    /**
     * Enregistre une action de type position. Idem que {@link #nouvelleAction(java.lang.String, int, int, int, int) },
     * mais on prend les même coordonnées que celles du Contexte.
     * @param nom nom de l'action
     * @param nbNoir nombre de pions noir après l'action
     * @param nbBlanc nombre de pions blancs après l'action
     */
    public void nouvelleAction(String nom, int nbNoir, int nbBlanc){
        nouvelleAction(nom + numVersCarLigne(x) + numVersCarColonne(y) + "," + nbNoir + "," + nbBlanc);
    }
    
    /**
     * Enregistre une action de type zone.
     * @param nom nom de l'action
     * @param direction direction de l'action
     * @param nbNoir nombre de pions noirs après l'action
     * @param nbBlanc nombre de pions blancs après l'action
     */
    public void nouvelleAction(String nom, char direction, int nbNoir, int nbBlanc){
        nouvelleAction(nom + direction + "," + nbNoir + "," + nbBlanc);
    }
    
    /**
     * Récupère les actions qui ont été enregistrées.
     * @return Un tableau de String contenant chacun une action.
     */
    public String[] actions(){
        return actions.toArray(new String[actions.size()]);
    }
    
    /**
     * Raccourci pour :
     * <pre>
     *  Contexte.estCouvrant(Contexte.plateau());
     * </pre>
     * <p>Cette méthode ne nécessite pas de reparcourir le plateau à chaque
     * appel.
     * @return <code>true</code> si le plateau est couvrant.
     * @see #estCouvrant(towa.Case[][]) Plus d'informations
     */
    public boolean estCouvrant(){
        return couvrant;
    }
    
    /**
     * Permet de savoir si un plateau est couvrant ou non.
     * <p>Un plateau est couvrant si chacune de ses lignes et de ses colonnes
     * comporte au moins un pion de chaque couleur.
     * <p>Attention, cette méthode recalcule le plateau pour chaque appel.
     * @param plateau le plateau à analyser
     * @return <code>true</code> si le plateau est couvrant.
     * @see #estCouvrant(int, int, boolean) Tester la couverture d'un tableau
     *                                      <i>après</i> une modification
     * @see #estCouvrant() Ne pas recalculer le plateau à chaque fois
     */
    public boolean estCouvrant(Case[][] plateau){
        System.out.print(" [Calcul de couverture ...");
        boolean estCouvrant = true;
        for(int i = 0; i < plateau.length && estCouvrant; i++){
            int nbNoir=0, nbBlanc=0;
            for(int j = 0; j < plateau.length; j++){
                Case c = plateau[i][j];     // LIGNES
                if(c.tourPresente){
                    if(c.estNoire)
                        nbNoir++;
                    else
                        nbBlanc++;
                }
            }
            if(nbNoir == 0 || nbBlanc == 0){
                System.out.print(" " + (nbNoir == 0 ? "N" : "B") + i + "?");
                estCouvrant = false;
            }
            nbNoir=0;
            nbBlanc=0;
            for(int j = 0; j < plateau.length; j++){
                Case c = plateau[j][i];     // COLONNES
                if(c.tourPresente){
                    if(c.estNoire)
                        nbNoir++;
                    else
                        nbBlanc++;
                }
            }
            if(nbNoir == 0 || nbBlanc == 0){
                System.out.print(" " + (nbNoir == 0 ? "N" : "B") + "?" + i);
                estCouvrant = false;
            }
        }
        System.out.print((estCouvrant ? " vrai" : " faux") + "]");
        return estCouvrant;
    }
    
    /**
     * Permet de tester si le tableau sera couvrant après une certaine modification.
     * <p>Le plateau considéré est celui contenu dans cet objet, accessible grâce
     * à {@link #plateau() plateau()}.
     * <p>Le plateau contenu dans cet objet n'est pas modifié.
     * @param ligne ligne de la case à ajouter
     * @param colonne colonne de la case à ajouter
     * @param estNoir couleur de la case à ajouter
     * @return <code>true</code> si le plateau est couvrant après cette modification.
     * @see #estCouvrant() Tester si le plateau actuel est couvrant
     */
    public boolean estCouvrant(int ligne, int colonne, boolean estNoir){
        final Case[][] plateauMod = new Case[plateau.length][plateau.length];
        for(int i = 0; i < plateau.length; i++)
            for(int j = 0; j < plateau.length; j++){
                if(i == ligne && j == colonne){
                    System.out.println("<Remplacement réussi : " + (estNoir ? "N" : "B") + " " + ligne + " " + colonne + ">");
                    plateauMod[i][j] = new Case(true, estNoir, 1, 1, 0);
                }else
                    plateauMod[i][j] = plateau[i][j];
            }
        return estCouvrant(plateauMod);
    }
}
