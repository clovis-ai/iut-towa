/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static towa.Utils.equalsCase;

/**
 *
 * @author icanet
 */
public class ZoneTest {
    
    Case[][] plateau;
    
    public ZoneTest() {
        String textePlateau = "A   B   C   D   E   F   G   H   I   J   K   L   M   N   O   P \n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "a|  4|  4|  1|  3|  1|  3|  1|  3|  3|  2|  2|  4|  1|  2|  3|  1|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "b|  1|  1|  4|  4|  1|  1|   |  2|  3|  1|  1|  1|   |  4|  2|  2|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "c|  1|  4|   |  2|  1|  4|   |  3|  2|N1 |  1|  2|  2|  1|N12|B43|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "d|  4|   |  1|  4|  2|   |   |  2|  1|  1|   |  4|  2|  4|  2|B44|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "e|   |N13|  2|  3|   |  4|B1 |   |B13|  1|  1|B1 |  4|B1 |B21|B21|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "f|  4|  2|   |  1|  4|  2|  4|   |   |  2|  3|  3|  2|  4|  2|  4|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "g|  1|   |  2|  3|  4|  3|  3|   |  2|  1|  1|  1|  4|  1|B1 |   |\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "h|  2|  2|   |  1|   |  4|  2|   |  1|  2|   |   |  1|  3|  1|  3|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "i|  3|  4|   |   |  2|  2|   |   |B21|  4|  1|N12|  1|  3|  3|   |\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "j|  2|  4|   |  3|N13|  1|  2|  1|   |B13|  1|  1|  4|N11|  3|   |\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "k|  1|  1|  4|  2|  4|  3|  3|  3|  1|  4|  1|  2|  1|   |   |  1|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "l|N42|  3|  3|   |  1|  4|  2|B13|  4|   |   |  3|  3|   |  3|  1|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "m|N43|N14|  1|   |  2|   |  4|  1|  4|N11|  1|  2|   |  3|B2 |  1|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "n|N14|N21|  2|B13|  2|  1|  3|  1|  4|   |   |  3|  2|  2|   |  3|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "o|  1|   |B11|  4|  2|N12|  4|  3|   |N22|  4|  3|  2|  1|  1|   |\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n" +
                            "p|  2|  2|   |   |  3|  2|  4|  2|  3|  4|  4|  3|  2|  3|  1|  4|\n" +
                            " +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+";
        plateau = Utils.plateauDepuisTexte(textePlateau);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of genererPorteeContour method, of class Zone.
     */
    @Test
    public void testGenererPorteeContour() {
        System.out.println("genererPorteeContour");
        Contexte ctx = new Contexte(plateau, true);
        Zone instance = new Zone(ctx.deplacement(0, 5));
        instance.genererPorteeContour();
        int s = 0;
        for(Case c : instance.portee())
            if(c != null)
                s++;
        assertEquals(0, s);
    }

    /**
     * Test of etendrePortee method, of class Zone.
     */
    @Test
    public void testEtendrePortee() {
        System.out.println("etendrePortee");
        Contexte ctx = new Contexte(plateau, true);
        Zone instance = new Zone(ctx.deplacement(2, 14));
        instance.genererPorteeContour();
        int s1 = 0;
        for(Case c : instance.portee())
            if(c != null)
                s1++;
        instance.etendrePortee();
        int s2 = 0;
        for(Case c : instance.portee())
            if(c != null)
                s2++;
        assertEquals(s1+2, s2);
    }

    /**
     * Test of trouverTour method, of class Zone.
     */
    @Test
    public void testTrouverTour_6args() {
        System.out.println("trouverTour");
        int ligne = 2;
        int colonne = 0;
        int dirX = 0;
        int dirY = 1;
        int maxDist = 16;
        Case expResult = Utils.caseDepuisCodage("N1 ");
        Case result = Zone.trouverTour(plateau, ligne+dirX, colonne+dirY, dirX, dirY, maxDist);
        assertTrue(equalsCase(expResult, result));
    }

    /**
     * Test of trouverTour method, of class Zone.
     */
    @Test
    public void testTrouverTour_4args() {
        System.out.println("trouverTour");
        Contexte ctx = new Contexte(plateau, true);
        ctx.deplacement(2, 0);
        int dirX = 0;
        int dirY = 1;
        int maxDist = 16;
        Case expResult = Utils.caseDepuisCodage("N1 ");
        Case result = Zone.trouverTour(ctx, dirX, dirY, maxDist);
        assertTrue(equalsCase(expResult, result));
    }

    /**
     * Test of portee method, of class Zone.
     */
    @Test
    public void testPortee_0args() {
        System.out.println("portee");
        Zone instance = null;
        Case[] expResult = null;
        Case[] result = instance.portee();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of portee method, of class Zone.
     */
    @Test
    public void testPortee_boolean() {
        System.out.println("portee");
        boolean contour = false;
        Zone instance = null;
        Case[] expResult = null;
        Case[] result = instance.portee(contour);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of estContour method, of class Zone.
     */
    @Test
    public void testEstContour() {
        System.out.println("estContour");
        Contexte ctx = new Contexte(plateau, true);
        Zone instance = new Zone(ctx.deplacement(0, 0));
        instance.portee(true);
        assertTrue(instance.estContour());
        instance.portee(false);
        assertFalse(instance.estContour());
    }
    
}
