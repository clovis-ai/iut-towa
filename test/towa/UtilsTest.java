/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package towa;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static towa.Utils.equalsCase;

/**
 *
 * @author icanet
 */
public class UtilsTest {
    
    public UtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of equalsCase method, of class Utils.
     */
    @Test
    public void testEqualsCase() {
        System.out.println("equalsCase");
        Case a = new Case(true, true, 0, 0, 0);
        Case b = new Case(true, true, 0, 0, 0);
        assertTrue (equalsCase(new Case(true, true, 0, 0, 0),
                               new Case(true, true, 0, 0, 0)));
        assertFalse(equalsCase(new Case(true, true, 0, 0, 0),
                               new Case(true, false, 0, 1, 0)));
        assertFalse(equalsCase(null,
                               new Case(true, true, 0, 0, 0)));
    }
    
}
